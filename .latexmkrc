# name of result pdf file
$$Pdest="result";
$jobname="rpz";

# bibtex
$bibtex_use = 1;
#$bibtex = 'biber %O %B';

$pdflatex="xelatex -shell-escape %O %S";
$pdflatex="xelatex %O %S";
$pdf_mode = 1; $postscript_mode = $dvi_mode = 0;
$out_dir= "output"; #$ENV{"TEMP"};
$silent = 1;

# либо preview_continuous_mode, либо preview_mode
$preview_continuous_mode = 1;
#$preview_mode= 1;


#$cleanup_mode = 1;

# for windows
$pdf_previewer = "start \"C:\\Program Files\\SumatraPDF\\SumatraPDF.exe\" %O %S";

