\chapter{Конструкторский раздел}
\section{Формулы для подъёмной силы и крутящих моментов}
Модель квадрокоптера имеет шесть степеней свободы и описывается уравнениями \ref{eq:velocity},
\ref{eq:eulerAnglesDeriv}, \ref{eq:velBodyDer}, \ref{eq:angSecondDer}. В этом разделе приводятся уравнения 
для крутящих моментов и подъёмной силы, действующих на летательный аппарат. Они изображены на
на рисунке \ref{fig:quadcopterForces}.

\begin{figure}[H]
  \centering
  \includegraphics[width=300]{res/png/quadcopterForces}
  \caption{Силы и крутящие моменты}
  \label{fig:quadcopterForces}
\end{figure}

Подъёмная сила и крутящие моменты винтов прямо пропорциональны квадрату их угловой скорости 
\cite{modelingAndContOfQuadcopter}. Формулы для силы тяги и крутящих моментов имеют следующий вид:

\begin{equation}
\begin{aligned}
    \tau_\phi &= lk(\omega_4^2 - \omega_2^2), \\
    \tau_\theta &= lk(\omega_3^2 - \omega_1^2), \\
    \tau_\psi &= b(-\omega_1^2 + \omega_2^2 - \omega_3^2 + \omega_4^2), \\
    F &= k(\omega_1^2 + \omega_2^2 + \omega_3^2 + \omega_4^2), \\
\end{aligned}
\end{equation}
где \(\omega_i\) --- угловая скорость вращения \(i\)-го винта, рад/c; \(k\) и \(b\) --- экспериментально определяемые постоянные.


В результате получена система уравнений \ref{eq:quadcopterOde}, которая описывает поведение квадрокоптера:

\begin{equation}
\begin{cases}
%    \dot{x} &= V_x \\
    \ddot{x} &= \frac{(C_\psi S_\theta C_\phi + S_\psi S_\phi)F - sign(\dot{x}) \cdot 0.5 c_d \rho \dot{x}^2 S_x}{m_t} \\
%    \dot{y} &= V_y \\
    \ddot{y} &=  \frac{S_\psi S_\theta C_\phi - C_\psi S_\phi)F - sign(\dot{y}) \cdot 0.5 c_d \rho \dot{y}^2 S_y}{m_t} \\
%    \dot{z} &= V_z \\
    \ddot{z} &= \frac{C_\phi C_\theta F - g - sign(\dot{z}) \cdot 0.5 c_d \rho \dot{z}^2 S_z}{m_t} \\
%    \dot{\phi} &= aV_\phi \\
    \ddot{\phi} &= \frac{\tau_\phi}{J_x} \\
%    \dot{\theta} &= aV_\theta \\
    \ddot{\theta} &= \frac{\tau_\theta}{J_y} \\
%    \dot{\psi} &= aV_\psi \\
    \ddot{\psi} &= \frac{\tau_\psi}{J_z} \\
    \tau_\phi &= lk(\omega_4^2 - \omega_2^2) \\
    \tau_\theta &= lk(\omega_3^2 - \omega_1^2) \\
    \tau_\psi &= b(-\omega_1^2 + \omega_2^2 - \omega_3^2 + \omega_4^2) \\
    F &= k(\omega_1^2 + \omega_2^2 + \omega_3^2 + \omega_4^2) \\
\end{cases}
\label{eq:quadcopterOde}
\end{equation}

\section{Алгоритм Рунге-Кутты}
\subsection{Случай одной переменной, которую дифференцируют}
Алгоритм Рунге-Кутты позволяет решать дифференциальные уравнения численно 
\cite{physicsSimulations}.

Рассмотрим дифференциальное уравнение с одной переменной дифференцирования

\begin{equation}
    x' = f(t,x)
\end{equation}
с начальным условием \(x(0) = x_0\). Предположим, что \(x_n\) --- значение переменной в момент времени 
\(t_n\). Формула Рунге-Кутты берёт \(x_n\) и \(t_n\) и вычисляет приближение для \(x_{n+1}\) в момент времени 
\(t_n + h\), где \(h\) --- шаг при изменении переменной \(t\). Используется взвешенное среднее 
приближенных значений \(f(t,x)\) в нескольких точках на интервале \((t_n, t_n+h)\). Формула для вычисления 
\(x_{n+1}\) имеет вид:
\begin{equation}
    x_{n+1} = x_n+ \frac{h}{6} (a + 2b + 2c +d),
\end{equation}
где
\begin{equation}
\begin{aligned}
    a &= f(t_n, x_n), \\
    b &= f(t_n + \frac{h}{2}, x_n + \frac{h}{2}a), \\
    c &= f(t_n + \frac{h}{2}, x_n + \frac{h}{2}b), \\
    d &= f(t_n + h, x_n+hc). \\
\end{aligned}
\end{equation}

Сначала на основе \(x_0\) находится \(x_1\), используя формулу выше. Потом переходим к \(x_1\), 
чтобы найти \(x_2\), и так далее.

В случае нескольких переменных алгоритм Рунге-Кутты выглядит так же, как и формулы, описанные выше,
но переменные заменяются векторами.

\subsection{Случай нескольких переменных, которые дифференцируют}
Предположим, что имеется \(m\) переменных \(x_1, x_2, ..., x_m\), каждая из которых изменяется по времени.
Предположим, что есть \(m\) дифференциальных уравнений для этих \(m\) переменных:

\begin{equation}
\begin{aligned}
    x'_1 &= f_1(x_1, x_2, ..., x_m), \\
    x'_2 &= f_2(x_1, x_2, ..., x_m), \\
         &\cdots \\
    x'_m &= f_m(x_1, x_2, ..., x_m). \\
\end{aligned}
\end{equation}

Заметим, что в правой части уравнений нет производных, в левой части только производные первого порядка.
Эти уравнения можно объединить в векторную форму

\begin{equation}
    \boldsymbol{\overline{x'}} = \boldsymbol{\overline{f}}(\boldsymbol{\overline{x}}),
\end{equation}
где \(\boldsymbol{\overline{x}} = (x_1, x_2, ..., x_m)\) и \(\boldsymbol{\overline{f}} = (f_1, f_2, ..., f_m)\).
Отметим состояния в некоторые моменты времени \(\boldsymbol{\overline{x}}_n, \boldsymbol{\overline{x}}_{n+1}\)
с шагом по времени \(h\). Таким образом, \(\boldsymbol{\overline{x}}_n\) --- значение \(m\) переменных
в момент времени \(t_m\). А \(x_{1,n}\) --- значение первой переменной \(x_1\) в момент времени \(t_n\).

\begin{equation}
\begin{aligned}
    \boldsymbol{\overline{x}}_n &= (x_{1,n}, x_{2,n}, ..., x_{m,n}), \\
    \boldsymbol{\overline{x}}_{n+1} &= (x_{1,n+1}, x_{2,n+1}, ..., x_{m,n+1}). \\
\end{aligned}
\end{equation}

Предположим, что \(\boldsymbol{\overline{x}}_n\) --- состояние системы в моменты времени \(t_n\). 
Чтобы вычислить состояние в момент \(t_{n+1} = t_n + h\) и поместить результат в 
\(\boldsymbol{\overline{x}}_{n+1}\) применим алгоритм Рунге-Кутты \cite{physicsSimulations}:

\begin{equation}
\begin{aligned}
    \boldsymbol{\overline{a}}_n &= \boldsymbol{\overline{f}}(\boldsymbol{\overline{x}}_n), \\
    \boldsymbol{\overline{b}}_n &= \boldsymbol{\overline{f}}(\boldsymbol{\overline{x}}_n + \frac{h}{2}\boldsymbol{\overline{a}}_n), \\
    \boldsymbol{\overline{c}}_n &= \boldsymbol{\overline{f}}(\boldsymbol{\overline{x}}_n + \frac{h}{2}\boldsymbol{\overline{b}}_n), \\
    \boldsymbol{\overline{d}}_n &= \boldsymbol{\overline{f}}(\boldsymbol{\overline{x}}_n + h\boldsymbol{\overline{c}}_n), \\
    \boldsymbol{\overline{x}}_{n+1} &= \boldsymbol{\overline{x}}_n + \frac{h}{6}(\boldsymbol{\overline{a}}_n + 2 \boldsymbol{\overline{b}}_n + 
2\boldsymbol{\overline{c}}_n + \boldsymbol{\overline{d}}_n).
\end{aligned}
\end{equation}

Выделим \(t\) как отдельную переменную \cite{physicsSimulations}, запишем полученную систему уравнений:

\begin{equation}
\begin{aligned}
    \boldsymbol{\overline{a}}_n &= \boldsymbol{\overline{f}}(t, \boldsymbol{\overline{x}}_n), \\
    \boldsymbol{\overline{b}}_n &= \boldsymbol{\overline{f}}(t+\frac{h}{2}, \boldsymbol{\overline{x}}_n + \frac{h}{2}\boldsymbol{\overline{a}}_n), \\
    \boldsymbol{\overline{c}}_n &= \boldsymbol{\overline{f}}(t+\frac{h}{2}, \boldsymbol{\overline{x}}_n + \frac{h}{2}\boldsymbol{\overline{b}}_n), \\
    \boldsymbol{\overline{d}}_n &= \boldsymbol{\overline{f}}(t + h, \boldsymbol{\overline{x}}_n + h\boldsymbol{\overline{c}}_n), \\
    \boldsymbol{\overline{x}}_{n+1} &= \boldsymbol{\overline{x}}_n + \frac{h}{6}(\boldsymbol{\overline{a}}_n + 2 \boldsymbol{\overline{b}}_n + 
2\boldsymbol{\overline{c}}_n + \boldsymbol{\overline{d}}_n).

\end{aligned}
\label{eq:rungeT}
\end{equation}

Преобразуем систему \ref{eq:quadcopterOde} к виду, пригодному для решения с использованием алгоритма Рунге-Кутты:

%\begin{equation}
\begin{cases}
    \dot{x} &= V_x \\
    \dot{V_x} &= \frac{1}{m_t}((C_\psi S_\theta C_\phi + S_\psi S_\phi)k(\omega_1^2 + \omega_2^2 + \omega_3^2 + \omega_4^2) - sign(V_x) \cdot 0.5 c_d \rho V_x^2 S_x) \\
    \dot{y} &= V_y \\
    \dot{V_y} &=  \frac{1}{m_t}(S_\psi S_\theta C_\phi - C_\psi S_\phi)k(\omega_1^2 + \omega_2^2 + \omega_3^2 + \omega_4^2) - sign(V_y) \cdot 0.5 c_d \rho V_y^2 S_y) \\
    \dot{z} &= V_z \\
    \dot{V_z} &= \frac{1}{m_t}(C_\phi C_\theta k(\omega_1^2 + \omega_2^2 + \omega_3^2 + \omega_4^2)  - sign(V_z) \cdot 0.5 c_d \rho V_z^2 S_z) -g \\
    \dot{\phi} &= V_\phi \\
    \dot{V_\phi} &= \frac{l}{J_x}k(\omega_4^2 - \omega_2^2) \\
    \dot{\theta} &= V_\theta \\
\dot{V_\theta} &= \frac{l}{J_y}k(\omega_3^2 - \omega_1^2) \\
    \dot{\psi} &= V_\psi \\
    \dot{V_\psi} &= \frac{b}{J_z}(-\omega_1^2 + \omega_2^2 - \omega_3^2 + \omega_4^2) \\
\end{cases}
\label{eq:quadcopterOdeConverted}
\end{equation}

Соотношения \ref{eq:rungeT} используются для решения системы дифференциальных уравнений 
\ref{eq:quadcopterOdeConverted}.

\section{Стабилизация квадрокоптера}

Для стабилизации квадрокоптера используется ПИД-регулятор. Преимуществами ПИД-регулятора являются
простая структура и лёгкость реализаци регулятора. Общие формулы для ПИД-регулятора :

\begin{equation}
\begin{aligned}
    e(t) &= x_d(t) - x(t), \\
    u(t) &= K_P e(t) + K_I \int \limits_0^t e(\tau) \mathrm{d} \tau + K_D \frac{\mathrm{d} e(t)}{\mathrm{d}t},
\end{aligned}
\end{equation}
где \(u(t)\) --- контрольный вход, \(e(t)\) --- разница между желаемым состоянием \(x_d(t)\) 
и текущим состоянием \(x(t)\), а \(K_P\), \(K_I\), $K_D$ --- пропоциональный, интегральный и дифференциальный коэффициенты соответственно.

\subsection{Схема метода}
\begin{figure}[H]
  \centering
  \includegraphics[width=250]{res/png/methodAlg}
  \caption{Схема метода стабилизации}
  \label{fig:simpleFuzzyPid}
\end{figure}


\subsection{Формулы для ПИД-регуляторов}
В квадрокоптере 6 состояний: координты $\xi$ и углы $\eta$, но только 4 контролирующих ввода: угловые
скорости четырёх винтов $\omega_i$. Взаимодействия между состояниями, общей подъёмной силой 
$F$, крутящими моментами $\tau$, создаваемыми винтами, видно из уравнений динамики квадрокоптера.
Общая подъёмная сила $F$ влияет на ускорение по оси $Oz$ и поддерживает квадрокоптер в воздухе. Крутящий момент
$\tau_\phi$ влияет на ускорение угла крена $\phi$, крутящий момент $\tau_\theta$ влияет на ускорение 
угла тангажа $\theta$, а крутящий момент $\tau_\psi$ --- на ускорение угла рыскания $\psi$.

Пусть $z_d$ --- координата $z$, которую нужно достичь. Пусть 
$z_d$ не изменяется в течение некоего промежутка времени, за который нужно изменить координату 
$z$ квадрокоптеру. Формула для ПИД-регулирования координаты $z$ будет иметь вид:

\begin{equation}
\begin{aligned}
    F &= (g + K_{z,P}(z_d - z) + K_{z,I} \int \limits_0^t (z_d -z)\mathrm{d}\tau + K_{z,D}\frac{\mathrm{d}(z_d - z)}{\mathrm{d}t})\frac{m_t}{C_\phi C_\theta} = \\
    &= (g + K_{z,P}(z_d - z) + K_{z,I} \int \limits_0^t (z_d -z)\mathrm{d}\tau - K_{z,D}\dot{z})\frac{m_t}{C_\phi C_\theta} \,.
\end{aligned}
\end{equation}

Пусть $\phi_d$ --- угол крена $\phi$, который нужно достичь. Пусть 
$\phi_d$ не изменяется в течение некоего промежутка времени, за который нужно изменить угол крена 
$\phi$ квадрокоптеру. Формула для ПИД-регулирования угла крена $\phi$ будет иметь вид:

\begin{equation}
\begin{aligned}
    \tau_\phi &= (K_{\phi,P}(\phi_d - \phi) + K_{\phi,I} \int \limits_0^t (\phi_d -\phi)\mathrm{d}\tau + K_{\phi,D}\frac{\mathrm{d}(\phi_d - \phi)}{\mathrm{d}t})J_x = \\
    &= (K_{\phi,P}(\phi_d - \phi) + K_{\phi,I} \int \limits_0^t (\phi_d -\phi)\mathrm{d}\tau - K_{\phi,D}\dot{\phi})J_x \,.
\end{aligned}
\end{equation}

Пусть $\theta_d$ --- угол тангажа $\theta$, который нужно достичь. Пусть 
$\theta_d$ не изменяется в течение некоего промежутка времени, за который нужно изменить угол тангажа
$\theta$ квадрокоптеру. Формула для ПИД-регулирования угла тангажа $\theta$ будет иметь вид:

\begin{equation}
\begin{aligned}
    \tau_\theta &= (K_{\theta,P}(\theta_d - \theta) + K_{\theta,I} \int \limits_0^t (\theta_d -\theta)\mathrm{d}\tau + K_{\theta,D}\frac{\mathrm{d}(\theta_d - \theta)}{\mathrm{d}t})J_y = \\
    &= (K_{\theta,P}(\theta_d - \theta) + K_{\theta,I} \int \limits_0^t (\theta_d -\theta)\mathrm{d}\tau - K_{\theta,D}\dot{\theta})J_y \,.
\end{aligned}
\end{equation}

Пусть $\psi_d$ --- угол рыскания $\psi$, который нужно достичь. Пусть 
$\psi_d$ не изменяется в течение некоего промежутка времени, за который нужно изменить угол рыскания 
$\psi$ квадрокоптеру. Формула для ПИД-регулирования угла рыскания $\psi$ будет иметь вид:

\begin{equation}
\begin{aligned}
    \tau_\psi &= (K_{\psi,P}(\psi_d - \psi) + K_{\psi,I} \int \limits_0^t (\psi_d -\psi)\mathrm{d}\tau + K_{\psi,D}\frac{\mathrm{d}(\psi_d - \psi)}{\mathrm{d}t})J_z = \\
    &= (K_{\psi,P}(\psi_d - \psi) + K_{\psi,I} \int \limits_0^t (\psi_d -\psi)\mathrm{d}\tau - K_{\psi,D}\dot{\psi})J_z \,.
\end{aligned}
\end{equation}

Для численного подсчёта интегралов используется метод трапеций.

Угловые скорости винтов $\ometa_i$ можно вычислить, используя формулы для нахождения подъёмной силы $F$,
крутящих моментов $\tau_\phi$, $\tau_\theta$, $\tau_\psi$:
\begin{equation}
\begin{aligned}
    \omega_1^2 &= \frac{F}{4k} - \frac{\tau_\theta}{2kl} - \frac{\tau_\psi}{4b}, \\
    \omega_2^2 &= \frac{F}{4k} - \frac{\tau_\phi}{2kl} + \frac{\tau_\psi}{4b}, \\
    \omega_3^2 &= \frac{F}{4k} + \frac{\tau_\theta}{2kl} - \frac{\tau_\psi}{4b}, \\
    \omega_4^2 &= \frac{F}{4k} + \frac{\tau_\phi}{2kl} + \frac{\tau_\psi}{4b}. \\

\end{aligned}
\end{equation}

\section{Нечёткий ПИД-регулятор}

Нечёткий ПИД-регулятор принимает на вход значение ошибки, скорость ошибки. Далее, используя заданные правила 
вывода, функции принадлежности, происходит расчёт коэффициентов $K_P$, $K_I$, $K_D$. После чего 
они направляются на вход к ПИД-регулятору. Структура показана на рисунке \ref{fig:simpleFuzzyPid}.

\begin{figure}[H]
  \centering
  \includegraphics[width=\textwidth]{res/png/simpleFuzzyPid}
  \caption{Структура контролируемой системы с регулятором}
  \label{fig:simpleFuzzyPid}
\end{figure}

 Также для расчёта коэффициентов задаются минимальные 
и максимальные значения для ошибки $e$, скорости ошибки $ed$, коэффициентов $K_P$,
$K_I$, $K_D$.

\subsection{Метод стабилизации на основе нечёткого вывода Мамдани}
Было реализовано нечёткое ПИД-регулирование с использованием алгоритма Мамдани для вывода значений 
коэффициентов ПИД-регулятора.

Для лингвистических переменных ошибки $e$ и скорости ошибки $ed$ было выбрано семь 
нечётких значений \cite{fuzzyPidMamdani}: 

\begin{enumerate}
    \item NB --- отрицательное большое;
    \item NM --- отрицательное среднее;
    \item NS --- отрицательное малое;
    \item ZO --- ноль;
    \item PS --- положительное малое;
    \item PM --- положительное среднее;
    \item PB --- положительное большое.
\end{enumerate}

Значения для трёх выходных лингвистических переменных,  соответствующих коэффициентам 
$K_P$, $K_I$, $K_D$:

\begin{enumerate}
    \item VVS --- очень очень малое;
    \item VS --- очень малое;
    \item S --- малое;
    \item M --- среднее;
    \item B --- большое;
    \item VB --- очень большое;
    \item VVB --- очень очень большое.
\end{enumerate}

Для получения чётких значений для коэффициентов ПИД-регулятора происходит дефуззификация соответствующих линвистических переменных.

Функции принадлежности для ошибки $e$, скорости ошибки $ed$ показаны на рисунке \ref{fig:mamdaniEdEMembershipFuncs}.

\begin{figure}[H]
  \centering
  \includegraphics[width=300]{res/png/mamdaniEdEMembershipFuncs}
  \caption{Функции принадлежности для ошибки и скорости ошибки}
  \label{fig:mamdaniEdEMembershipFuncs}
\end{figure}

Функции принадлежности для коэффициентов $K_P$, $K_I$, $K_D$ показаны на рисунке \ref{fig:mamdaniKMembershipFuncs}.

\begin{figure}[H]
  \centering
  \includegraphics[width=300]{res/png/mamdaniKMembershipFuncs}
  \caption{Функции принадлежности для $K_P$, $K_I$, $K_D$}
  \label{fig:mamdaniKMembershipFuncs}
\end{figure}

Приходящие значения ошибки и скорости ошибки преобразуются в значения в интервале $[-1, \, 1]$ по формуле:

\begin{equation}
    x_{[-1, \, 1]} = -1 + \frac{2(x - x_{min})}{x_{max} - x_{min}},
\end{equation}
где $x$ --- значение из интервала $[x_{min}, \, x_{max}]$, $x_{min}$ --- минимальное значение $x$, $x_{max}$ --- максимальное значение $x$,
$x_{[-1, \, 1]}$ --- значение в интервале $[-1, \, 1]$.

После дефуззификации коэффициенты ПИД-регулятора нужно преобразовать из интервала $[-1, \, 1]$ к необходимым значениям, используя соответствующие минимальные и максимальные значения:

\begin{equation}
    x = x_{min} + \frac{(x_{[-1, \,1]} + 1)(x_{max} - x_{min})}{2},
\end{equation}
где $x$ --- значение из интервала $[x_{min}, \, x_{max}]$, $x_{min}$ --- минимальное значение $x$, $x_{max}$ --- максимальное значение $x$,
$x_{[-1, \, 1]}$ --- значение в интервале $[-1, \, 1]$.

Правила вывода для получения коэффициентов $K_P$, $K_I$ из ошибки $e$ и скорости ошибки $de$
показаны в таблице \ref{tab:rulesMamdaniKpKi}: 

\begin{table}[ht]
\caption{Правила вывода для $K_P$ и $K_I$}
\begin{center}
\begin{tabular}{ |c|l|l|l|l|l|l|l|l| }
    \hline
    & \multicolumn{8}{|c|}{Ошибка} \\
    \hline
    \parbox[t]{2mm}{\multirow{8}{*}{\rotatebox[origin=c]{90}{Скорость ошибки }}} 
    & & \textbf{NB} & \textbf{NM} & \textbf{NS} & \textbf{ZO} & \textbf{PS} & \textbf{PM} & \textbf{PB} \\ 
    \cline{2-9}
    & \textbf{NB} & M & S & VS & VVS & VS & S & M \\
    \cline{2-9}
    & \textbf{NM} & B & M & S & VS & S & M & B \\
    \cline{2-9}
    & \textbf{NS} & VB & B & M & S & M & B & VB \\
    \cline{2-9}
    & \textbf{ZO} & VVB & VB & B & M & B & VB & VVB \\
    \cline{2-9}
    & \textbf{PS} & VB & B & M & S & M & B & VB \\
    \cline{2-9}
    & \textbf{PM} & B & M & S & VS & S & M & B \\
    \cline{2-9}
    & \textbf{PB} & M & S & VS & VVS & VS & S & M \\
    \hline
\end{tabular}
\end{center}
\label{tab:rulesMamdaniKpKi}
\end{table}

Правила вывода для получения коэффициента $K_D$ из ошибки $e$ и скорости ошибки $de$
показаны в таблице \ref{tab:rulesMamdaniKd}: 

\begin{table}[H]
\caption{Правила вывода для $K_D$}
\begin{center}
\begin{tabular}{ |c|l|l|l|l|l|l|l|l| }
    \hline
    & \multicolumn{8}{|c|}{Ошибка} \\
    \hline
    \parbox[t]{2mm}{\multirow{8}{*}{\rotatebox[origin=c]{90}{Скорость ошибки }}} 
    & & \textbf{NB} & \textbf{NM} & \textbf{NS} & \textbf{ZO} & \textbf{PS} & \textbf{PM} & \textbf{PB} \\ 
    \cline{2-9}
    & \textbf{NB} & M & B & VB & VVB & VB & B & M \\
    \cline{2-9}
    & \textbf{NM} & S & M & B & VB & B & M & S \\
    \cline{2-9}
    & \textbf{NS} & VS & S & M & B & M & S & VS \\
    \cline{2-9}
    & \textbf{ZO} & VVS & VS & S & M & S & VS & VVS \\
    \cline{2-9}
    & \textbf{PS} & VS & S & M & B & M & S & VS \\
    \cline{2-9}
    & \textbf{PM} & S & M & B & VB & B & M & S \\
    \cline{2-9}
    & \textbf{PB} & M & B & VB & VVB & VB & B & M \\
    \hline
\end{tabular}
\end{center}
\label{tab:rulesMamdaniKd}
\end{table}

Схема алгоритма нахождения коэффициента ПИД-регулятора с использованием нечёткого вывода Мамдани показана на рисунке \ref{fig:flowchartMamdaniStab}.

\newpage
\begin{figure}[H]
  \centering 
  \includegraphics[height=650]{res/png/flowchartMamdaniStab}
  \caption{Нахождение коэффициента ПИД-регулятора с нечётким выводом Мамдани}
  \label{fig:flowchartMamdaniStab}
\end{figure}

%\begin{figure}[H]
%  \centering
%  \includesvg{flowchartMamdaniStab}
%  \caption{Стабилизация с применением вывода Мамдани}
%  \label{fig:flowchartMamdaniStab}
%\end{figure}


\subsection{Метод стабилизации на основе нечёткого вывода Сугено}
Было реализовано нечёткое ПИД-регулирование с использованием алгоритма Сугено для
вывода значений коэффициентов ПИД-регулятора.

Входные лингвистические переменные те же самые, что и в алгоритме Мамдани,
функции их распределения такие же, как и в алгоритме Мамдани.
В результате использования алгоритма Сугено на выходе будут получены чёткие значения коэффициентов $K_P$, $K_I$,
$K_D$, этап дефуззификации не нужен.

Значения, которые коэффициенты $K_P$ и $K_D$ могут принимать на выходе алгоритма Сугено:
\begin{enumerate}
    \item S = 0.1;
    \item B = 0.9.
\end{enumerate}

Значения, которые коэффициент $K_I$ может принимать на выходе алгоритма Сугено:

\begin{enumerate}
    \item ZO = 0;
    \item S = 0.3;
    \item M = 0.7;
    \item B = 1.
\end{enumerate}

Правила вывода для получения коэффициентов $K_P$ из ошибки $e$ и скорости ошибки $de$
показаны в таблице \ref{tab:rulesSugenoKp}: 

\begin{table}[ht]
\caption{Правила вывода для $K_P$ в алгоритме Сугено}
\begin{center}
\begin{tabular}{ |c|l|l|l|l|l|l|l|l| }
    \hline
    & \multicolumn{8}{|c|}{Скорость ошибки} \\
    \hline
    \parbox[t]{2mm}{\multirow{8}{*}{\rotatebox[origin=c]{90}{Ошибка}}} 
    & & \textbf{NB} & \textbf{NM} & \textbf{NS} & \textbf{ZO} & \textbf{PS} & \textbf{PM} & \textbf{PB} \\ 
    \cline{2-9}
    & \textbf{NB} & B & B & B & B & B & B & B \\
    \cline{2-9}
    & \textbf{NM} & S & B & B & B & B & B & S \\
    \cline{2-9}
    & \textbf{NS} & S & S & B & B & B & S & S \\
    \cline{2-9}
    & \textbf{ZO} & S & S & S & B & S & S & S \\
    \cline{2-9}
    & \textbf{PS} & S & S & B & B & B & S & S \\
    \cline{2-9}
    & \textbf{PM} & S & B & B & B & B & B & S \\
    \cline{2-9}
    & \textbf{PB} & B & B & B & B & B & B & B \\
    \hline
\end{tabular}
\end{center}
\label{tab:rulesSugenoKp}
\end{table}

Правила вывода для получения коэффициента $K_I$ из ошибки $e$ и скорости ошибки $de$
показаны в таблице \ref{tab:rulesSugenoKi}: 

\begin{table}[H]
\caption{Правила вывода для $K_I$ в алгоритме Сугено}
\begin{center}
\begin{tabular}{ |c|l|l|l|l|l|l|l|l| }
    \hline
    & \multicolumn{8}{|c|}{Скорость ошибки} \\
    \hline
    \parbox[t]{2mm}{\multirow{8}{*}{\rotatebox[origin=c]{90}{Ошибка}}} 
    & & \textbf{NB} & \textbf{NM} & \textbf{NS} & \textbf{ZO} & \textbf{PS} & \textbf{PM} & \textbf{PB} \\ 
    \cline{2-9}
    & \textbf{NB} & S & S & S & S & S & S & S \\
    \cline{2-9}
    & \textbf{NM} & B & B & S & S & S & B & B \\
    \cline{2-9}
    \cline{2-9}
    & \textbf{ZO} & B & B & B & B & B & B & B \\
    \cline{2-9}
    & \textbf{PS} & B & B & B & S & B & B & B \\
    \cline{2-9}
    & \textbf{PM} & B & B & S & S & S & B & B \\
    \cline{2-9}
    & \textbf{PB} & S & S & S & S & S & S & S \\
    \hline
\end{tabular}
\end{center}
\label{tab:rulesSugenoKi}
\end{table}

Правила вывода для получения коэффициента $K_D$ из ошибки $e$ и скорости ошибки $de$
показаны в таблице \ref{tab:rulesSugenoKd}: 

\begin{table}[H]
\caption{Правила вывода для $K_D$ в алгоритме Сугено}
\begin{center}
\begin{tabular}{ |c|l|l|l|l|l|l|l|l| }
    \hline
    & \multicolumn{8}{|c|}{Скорость ошибки} \\
    \hline
    \parbox[t]{2mm}{\multirow{8}{*}{\rotatebox[origin=c]{90}{Ошибка}}} 
    & & \textbf{NB} & \textbf{NM} & \textbf{NS} & \textbf{ZO} & \textbf{PS} & \textbf{PM} & \textbf{PB} \\ 
    \cline{2-9}
    & \textbf{NB} & B & B & B & B & B & B & B \\
    \cline{2-9}
    & \textbf{NM} & M & M & B & B & B & M & M \\
    \cline{2-9}
    & \textbf{NS} & S & M & M & B & M & M & S \\
    \cline{2-9}
    & \textbf{ZO} & ZO & S & M & B & M & S & ZO \\
    \cline{2-9}
    & \textbf{PS} & S & M & M & B & M & M & S \\
    \cline{2-9}
    & \textbf{PM} & M & M & B & B & B & M & M \\
    \cline{2-9}
    & \textbf{PB} & B & B & B & B & B & B & B \\
    \hline
\end{tabular}
\end{center}
\label{tab:rulesSugenoKd}
\end{table}

Схема алгоритма нахождения коэффициента ПИД-регулятора с использованием нечёткого вывода Сугено показана на рисунке \ref{fig:flowchartSugenoStab}.

\begin{figure}[H]
  \centering
  \includegraphics[height=650]{res/png/flowchartSugenoStab}
  \caption{Нахождение коэффициента ПИД-регулятора с нечётким выводом Сугено}
  \label{fig:flowchartSugenoStab}
\end{figure}

\section{IDEF0-диаграмма}

На рисунке \ref{fig:idef0QuadcopterSim} показана IDEF0-диаграмма расчёта положения квадрокоптера в течение времени $t_{max}$. 

\begin{figure}[H]
  \centering
  \includegraphics[width=\textwidth]{res/png/idef0QuadcopterSim}
  \caption{}
  \label{fig:idef0QuadcopterSim}
\end{figure}

\section{Диапазоны регулируемых величин}
Координата $z$ может находиться в $[-5; 5]$. Крен, тангаж и рыскание измеряются в градусах и 
находятся в $[-45; 45]$.
