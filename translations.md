roll - крен
pitch - тангаж
yaw - рыскание
closed-loop control system - замкнутая система регулирования
stability - устойчивость
gyro - гироскоп
rotational rate - скорость вращения
Proportional Gain coefficient - пропорциональный коэффициент усиления
sluggish - медлительный
steady - устойчивый
oscillate - качаться, колебаться
counteract - противодействовать
set-point - уставка
throttle - дроссель, тормоз, регулятор
wobble - качаться
jerkiness - подёргивание
rush - торопиться
thrust - подъёмная сила
closed-loop system - замкнутая система
attitude - угловое положение
altitude - высота
torque - крутящий момент, момент силы
four arms (inertia) - главные оси инерции
inertia matrix - тензор инерции
rigid body - жёсткое тело
centrifugal force - центробежная сила
centripetal force - центростремительная сила
gyroscopic forces - гироскопические силы


